var Twit = require('twit')
require('dotenv').load()

var T = new Twit({
  consumer_key: process.env.CONSUMER_KEY,
  consumer_secret: process.env.CONSUMER_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  access_token_secret: process.env.ACCESS_TOKEN_SECRET
})

var retweet = function (tweet_id) {
  T.post('statuses/retweet/:id', { id: tweet_id }, function (err, data, res) {
    if (err) {
      console.error(`${ err } (${ tweet_id })`)
      return
    }

    console.log(`Successfully retweeted: ${ data.text }`)
  })
}

var favorite = function (tweet_id) {
  T.post('favorites/create', { id: tweet_id }, function (err, data, res) {
    if (err) {
      console.error(`${ err } (${ tweet_id })`)
      return
    }

    console.log(`Successfully favorited: ${ data.text }`)
  })
}

var follow = function (user_id) {
  T.post('friendships/create', { id: user_id }, function (err, data, res) {
    if (err) {
      console.error(`${ err } (${ user_id })`)
      return
    }

    console.log(`Successfully followed: @${ data.screen_name }`)
  })
}

var stream = T.stream('statuses/filter', { track: 'weakness' })
stream.on('tweet', function (tweet) {
  if (tweet.text.search(/my greatest weakness/i) >= 0 || 
      tweet.text.search(/my one weakness/i) >= 0) {
    console.log(`Received tweet ${ tweet.id_str }.`)
    var retweeted_tweet = 
      (tweet.retweeted_status !== undefined)

    /* filter out bot's tweets */
    if (tweet.user.screen_name === 'myoneweaknesses' ||
      (retweeted_tweet && tweet.retweeted_status.user.screen_name === 'myoneweaknesses'))
      return

    /* filter out sensitive content */
    var sensitive = (tweet.possibly_sensitive || 
      (retweeted_tweet && tweet.retweeted_status.possibly_sensitive))
    if (sensitive)
      return

    /* don't attempt to retweet a tweet that's already been retweeted */
    if (tweet.retweeted ||
       (retweeted_tweet && tweet.retweeted_status.retweeted))
      return
    
    retweet(tweet.id_str)

    if (!tweet.user.following)
      follow(tweet.user.id_str)
    if (retweeted_tweet && !tweet.retweeted_status.user.following)
      follow(tweet.retweeted_status.user.id_str)
  }
})

console.log('Listening for tweets.')
