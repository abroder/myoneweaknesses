# my one weaknesses

@[myoneweaknesses](http://twitter.com/myoneweaknesses)

Retweets anyone who uses the phrase "my one weakness" or "my greatest 
weaknesses." It makes an effort to filter out tweets with sensitive content,
but it's not perfect yet, so I occasionally do a bit of manual curation.